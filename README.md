# Life in the Woods Renaissance Server

## build

`docker build -t ${USER}_litwr .`

## first run

`docker run --rm -it --name ${USER}-litwr -p=25565:25565 -p=25565:25565/udp -v=/data/litwr:/litwr ${USER}_litwr /firstrun`

after server finished starting type `stop` into the console to stop it
server is now ready

## normal start

`screen docker run --rm -it --name ${USER}-litwr -p=25565:25565 -p=25565:25565/udp -v=/data/litwr:/litwr ${USER}_litwr`

or

`./launch.sh`
