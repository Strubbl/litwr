FROM ubuntu:16.04
LABEL maintainer "Strubbl"

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get -y install default-jdk unzip wget

RUN addgroup --gid 1000 minecraft
RUN adduser --shell /bin/false --uid 1000 --ingroup minecraft --home /home/minecraft minecraft \
  && mkdir /data \
  && chown minecraft:minecraft /data /home/minecraft

WORKDIR /data

ADD ./start /start
ADD ./firstrun /firstrun
RUN chmod +x /start /firstrun

ENV UID=1000 GID=1000
USER minecraft

EXPOSE 25565
VOLUME ["/data"]
CMD    ["/start"]


